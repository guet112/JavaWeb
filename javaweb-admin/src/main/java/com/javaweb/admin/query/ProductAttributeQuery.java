// +----------------------------------------------------------------------
// | JavaWeb混编版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

/**
 * <p>
 * 商品属性查询条件
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-06-08
 */
@Data
public class ProductAttributeQuery extends BaseQuery {

    /**
     * 属性名称
     */
    private String name;

    /**
     * 属性的类型：1规格 2属性
     */
    private Integer type;

}
