// +----------------------------------------------------------------------
// | JavaWeb混编版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.admin.query;

import com.javaweb.common.common.BaseQuery;
import lombok.Data;

/**
 * <p>
 * 运费模版查询条件
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-06-09
 */
@Data
public class FeightTemplateQuery extends BaseQuery {

    /**
     * 模板名称
     */
    private String name;

    /**
     * 计费类型：1按重量 2按件数
     */
    private Integer chargeType;

}
