// +----------------------------------------------------------------------
// | JavaWeb混编版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.admin.service;

import com.javaweb.admin.entity.ProductCategory;
import com.javaweb.common.common.IBaseService;

/**
 * <p>
 * 商品分类 服务类
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-05-18
 */
public interface IProductCategoryService extends IBaseService<ProductCategory> {

}