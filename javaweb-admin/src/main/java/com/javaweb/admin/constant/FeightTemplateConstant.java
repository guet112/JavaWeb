// +----------------------------------------------------------------------
// | JavaWeb混编版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.admin.constant;

import java.util.HashMap;
import java.util.Map;

/**
 * <p>
 * 运费模版 模块常量
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-06-09
 */
public class FeightTemplateConstant {

    /**
     * 计费类型
     */
    public static Map<Integer, String> FEIGHTTEMPLATE_CHARGETYPE_LIST = new HashMap<Integer, String>() {
        {
            put(1, "按重量");
            put(2, "按件数");
        }
    };
}