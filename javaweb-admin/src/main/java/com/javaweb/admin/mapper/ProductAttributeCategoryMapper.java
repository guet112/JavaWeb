// +----------------------------------------------------------------------
// | JavaWeb混编版框架 [ JavaWeb ]
// +----------------------------------------------------------------------
// | 版权所有 2019~2020 南京JavaWeb研发中心
// +----------------------------------------------------------------------
// | 官方网站: http://www.javaweb.vip/
// +----------------------------------------------------------------------
// | 作者: 鲲鹏 <1175401194@qq.com>
// +----------------------------------------------------------------------

package com.javaweb.admin.mapper;

import com.javaweb.admin.entity.ProductAttributeCategory;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 产品属性分类 Mapper 接口
 * </p>
 *
 * @author 鲲鹏
 * @since 2020-06-08
 */
public interface ProductAttributeCategoryMapper extends BaseMapper<ProductAttributeCategory> {

}
